﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Ingredient : MonoBehaviour
{
    private Vector3 _startingPosition;
    private Quaternion _startingRotation;
    private Collider _collider;

    private void OnEnable()
    {
        GameManager.OnReset += Reset;
    }

    private void OnDisable()
    {
        GameManager.OnReset -= Reset;
    }

    private void Start()
    {
        var thisTransform = transform;
        _startingPosition = thisTransform.position;
        _startingRotation = thisTransform.rotation;
        _collider = GetComponent<Collider>();
    }

    private void Reset()
    {
        if (transform.parent)
        {
            var objParent = transform.parent.gameObject;
            Destroy(objParent, 1f);
        }

        var thisTransform = transform;
        thisTransform.parent = null;
        thisTransform.position = _startingPosition;
        thisTransform.rotation = _startingRotation;
        _collider.enabled = true;
    }
}