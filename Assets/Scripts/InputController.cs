﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InputController : MonoBehaviour
{
    private const int LayerMask = 1 << 8; // Layer 8 is for Ingredients only

    private Transform _sandwichHolder;
    private Vector3 _topSandwichIngredientPos = Vector3.zero;
    private Vector3 _targetPos;
    private GameObject _selectedIngredient;
    private GameObject _newSandwichIngredient;
    [SerializeField] private GridSpawnGenerator gridSpawnGenerator;
    private Camera _camera;

    public GameObject TopIngredient { get; private set; }
    public GameObject BottomIngredient { get; private set; }

    private void Awake()
    {
        _camera = Camera.main;

        if (_camera == null)
            _camera = (Camera) FindObjectOfType(typeof(Camera));

        if (_camera == null)
        {
            Debug.LogWarning("No camera found in the current scene");
            this.enabled = false;
        }
    }

    private void OnEnable()
    {
        SwipeDetector.OnSwipe += CheckInput;
        GameManager.OnReset += Reset;
    }

    private void OnDisable()
    {
        SwipeDetector.OnSwipe -= CheckInput;
        GameManager.OnReset -= Reset;
    }

    private void CheckInput(SwipeData data)
    {
        var ray = _camera.ScreenPointToRay(data.EndPosition);
        if (Physics.Raycast(ray, out var hit, 100f, LayerMask))
            _selectedIngredient = hit.collider.gameObject;

        else return;

        var selectedIngredientPos = _selectedIngredient.transform.position;

        switch (data.Direction)
        {
            case SwipeDirection.Up:
            {
                if ((int) selectedIngredientPos.z == gridSpawnGenerator.GridZ - 1)
                    break;

                var rotationPivot = new Vector3(selectedIngredientPos.x, selectedIngredientPos.y,
                    _selectedIngredient.transform.localScale.z / 2);
                _targetPos = new Vector3(selectedIngredientPos.x, 0, selectedIngredientPos.z + 1);
                CheckMove(Vector3.right, rotationPivot);
                break;
            }

            case SwipeDirection.Right:
            {
                if ((int) selectedIngredientPos.x == gridSpawnGenerator.GridX - 1)
                    break;

                var rotationPivot = new Vector3(_selectedIngredient.transform.localScale.z / 2,
                    selectedIngredientPos.y, selectedIngredientPos.z);
                _targetPos = new Vector3(selectedIngredientPos.x + 1, 0, selectedIngredientPos.z);
                CheckMove(Vector3.back, rotationPivot);
                break;
            }

            case SwipeDirection.Down:
            {
                if ((int) selectedIngredientPos.z == 0)
                    break;

                var rotationPivot = new Vector3(selectedIngredientPos.x, selectedIngredientPos.y,
                    -_selectedIngredient.transform.localScale.z / 2);
                _targetPos = new Vector3(selectedIngredientPos.x, 0, selectedIngredientPos.z - 1);
                CheckMove(Vector3.left, rotationPivot);
                break;
            }

            case SwipeDirection.Left:
            {
                if ((int) selectedIngredientPos.x == 0)
                    break;

                var rotationPivot = new Vector3(-_selectedIngredient.transform.localScale.z / 2,
                    selectedIngredientPos.y, selectedIngredientPos.z);
                _targetPos = new Vector3(selectedIngredientPos.x - 1, 0, selectedIngredientPos.z);
                CheckMove(Vector3.forward, rotationPivot);
                break;
            }
        }
    }

    private void CheckMove(Vector3 axis, Vector3 pivot)
    {
        var canMove = CheckIngredientsPosition(_targetPos);
        if (!canMove) return;
        SetPileParent(pivot);
        StartCoroutine(FlipPile(axis, -180, 0.5f));
    }

    private void SetPileParent(Vector3 pivot)
    {
        var pileParent = new GameObject("Sandwich");
        if (_sandwichHolder == null)
            _sandwichHolder = pileParent.transform;

        pileParent.transform.position = _selectedIngredient.transform.position + pivot;

        if (!_selectedIngredient.transform.parent)
        {
            _selectedIngredient.transform.parent = pileParent.transform;
            return;
        }

        var ingredientsParent = _selectedIngredient.transform.parent;
        var ingredients = (from Transform child in _selectedIngredient.transform.parent select child.gameObject)
            .ToList();

        foreach (var go in ingredients)
            go.transform.parent = pileParent.transform;

        Destroy(ingredientsParent.gameObject);
    }

    private bool CheckIngredientsPosition(Vector3 pos)
    {
        var spawnedIngredients = gridSpawnGenerator.SpawnedIngredients;
        foreach (var ingredient in spawnedIngredients.Where(ingredient =>
            (int) ingredient.transform.position.x == (int) pos.x &&
            (int) ingredient.transform.position.z == (int) pos.z))
        {
            _newSandwichIngredient = ingredient.gameObject;
            return true;
        }

        return false; // No ingredient found in the target position (swipe direction)
    }

    private IEnumerator FlipPile(Vector3 axis, float angle, float duration = 1.0f)
    {
        var rotation = _selectedIngredient.transform.parent.rotation;
        var @from = rotation;
        var to = rotation;
        to *= Quaternion.Euler(axis * angle);
        var elapsed = 0.0f;

        while (elapsed < duration)
        {
            _selectedIngredient.transform.parent.rotation = Quaternion.Slerp(from, to, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }

        _selectedIngredient.transform.parent.rotation = to;
        var ingredientsToPile = new List<GameObject>();

        if (!_newSandwichIngredient.transform.parent)
        {
            _selectedIngredient.transform.position = _targetPos + Vector3.up / 5;
            _newSandwichIngredient.transform.parent = _selectedIngredient.transform.parent;
        }
        else
        {
            var oldParent = _selectedIngredient.transform.parent;
            ingredientsToPile.AddRange(from Transform ingredient in _selectedIngredient.transform.parent
                select ingredient.gameObject);

            _topSandwichIngredientPos = TopIngredient.transform.position;
            _topSandwichIngredientPos -= Vector3.up / 10;

            foreach (var ingredient in ingredientsToPile)
            {
                ingredient.transform.position = _topSandwichIngredientPos;
                ingredient.transform.parent = _newSandwichIngredient.transform.parent;
            }

            Destroy(oldParent.gameObject);
        }

        var childCount = _selectedIngredient.transform.parent.childCount;

        if (_selectedIngredient.transform.parent.childCount == 2)
        {
            TopIngredient = _selectedIngredient.transform.parent.GetChild(0).gameObject;
            BottomIngredient = _selectedIngredient.transform.parent.GetChild(1).gameObject;
        }

        else
        {
            TopIngredient = _selectedIngredient.transform.parent.GetChild(childCount - 2).gameObject;
            BottomIngredient = _selectedIngredient.transform.parent.GetChild(childCount - 1).gameObject;
        }

        _selectedIngredient.GetComponent<Collider>().enabled = _selectedIngredient == TopIngredient;
        GameManager.Instance.PiecesToPile();
    }

    private void Reset()
    {
        _selectedIngredient = null;
    }
}