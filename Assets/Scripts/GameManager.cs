﻿using UnityEngine;

[RequireComponent(typeof(InputController))]
public class GameManager : MonoBehaviour
{
    private int _piecesToPile;
    [SerializeField] private GridSpawnGenerator gridSpawnGenerator;
    private InputController _inputController;

    public delegate void ResetAction();
    public static event ResetAction OnReset;
    public static GameManager Instance;

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else
            Destroy(this.gameObject);

        _inputController = GetComponent<InputController>();
    }

    public void SetPieces(int pieces)
    {
        _piecesToPile = pieces;
    }

    public void PiecesToPile()
    {
        _piecesToPile--;
        if (_piecesToPile == 0)
            CheckWinCondition();
    }

    private void CheckWinCondition()
    {
        if (_piecesToPile != 0) return;
        if (_inputController.TopIngredient.CompareTag("Bread") && _inputController.BottomIngredient.CompareTag("Bread"))
            Debug.Log("YOU WIN");
    }


    public void ResetGame()
    {
        OnReset?.Invoke();
        _piecesToPile = gridSpawnGenerator.SpawnedIngredients.Count;
    }
}