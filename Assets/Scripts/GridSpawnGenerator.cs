﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GridSpawnGenerator : MonoBehaviour
{
    private const byte BreadPieces = 2;
    private const byte MinIngredientsToSpawn = 2;

    [Header("Spawning Objects")] [SerializeField]
    private Ingredient bread;
    [SerializeField] private Ingredient[] ingredients; // Using Collider to ensure that an ingredient has it 

    [Header("Grid properties")] 
    [SerializeField] private GameObject gridTile;
    [SerializeField] [Tooltip("Number of rows")]
    private int gridX; // Number of grid rows
    [SerializeField] [Tooltip("Number of columns")]
    private int gridZ; // Number of grid columns 

    private Vector3 _lastIngredientPos = Vector3.zero;
    private readonly List<Vector3> _emptyGridCells = new List<Vector3>(); // Used to check available cells in the grid
    public readonly List<Ingredient> SpawnedIngredients = new List<Ingredient>();

    public int GridX => gridX;
    public int GridZ => gridZ;

    private void Start()
    {
        CreateGrid();
        SpawnIngredients();
    }

    private void CreateGrid()
    {
        var gridParent = new GameObject("Grid");

        for (var x = 0; x < gridX; x++)
        {
            for (var z = 0; z < gridZ; z++)
            {
                var spawnPosition = new Vector3(x, 0, z);
                var go = Instantiate(gridTile, spawnPosition, gridTile.transform.rotation);
                go.transform.parent = gridParent.transform;
                _emptyGridCells.Add(spawnPosition);
            }
        }
    }

    private void SpawnIngredients()
    {
        if (!bread || ingredients.Length < MinIngredientsToSpawn) return;
        var randomObjectsToSpawn = Random.Range(MinIngredientsToSpawn, gridX * gridZ - BreadPieces);
        GameManager.Instance.SetPieces(randomObjectsToSpawn);

        var breadPieces = new List<GameObject>();

        // Spawn bread pieces
        for (var i = 0; i < BreadPieces; i++)
        {
            _lastIngredientPos = GetRandomGridCellPosition();
            var newIngredient = Instantiate(bread, _lastIngredientPos, bread.transform.rotation);
            SpawnedIngredients.Add(newIngredient);
            _emptyGridCells.Remove(_lastIngredientPos);
            breadPieces.Add(newIngredient.gameObject);
        }

        var randomIngredientId = 0;
        var currentIngredientId = 0;

        // Spawn sandwich ingredients
        for (var i = 0; i < randomObjectsToSpawn; i++)
        {
            _lastIngredientPos = GetRandomGridCellPosition();

            // This is to ensure that at least 2 different ingredients are present
            while (randomIngredientId == currentIngredientId)
                randomIngredientId = Random.Range(0, ingredients.Length);

            currentIngredientId = randomIngredientId;
            var selectedIngredient = ingredients[randomIngredientId];
            var newIngredient =
                Instantiate(selectedIngredient, _lastIngredientPos, selectedIngredient.transform.rotation);
            SpawnedIngredients.Add(newIngredient);
            _emptyGridCells.Remove(_lastIngredientPos);
        }

        foreach (var breadPiece in breadPieces)
        {
            breadPiece.transform.position += Vector3.up / 10;
        }
    }

    private Vector3 GetRandomGridCellPosition()
    {
        if (SpawnedIngredients.Count == 0)
            return _emptyGridCells[Random.Range(0, _emptyGridCells.Count)];

        foreach (var ingredientPos in SpawnedIngredients.Select(ingredient => ingredient.transform.position))
        {
            var targetPos = ingredientPos + Vector3.right;
            if (_emptyGridCells.Contains(targetPos)) return targetPos;

            targetPos = ingredientPos + Vector3.left;
            if (_emptyGridCells.Contains(targetPos)) return targetPos;

            targetPos = ingredientPos + Vector3.forward;
            if (_emptyGridCells.Contains(targetPos)) return targetPos;

            targetPos = ingredientPos + Vector3.back;
            if (_emptyGridCells.Contains(targetPos)) return targetPos;
        }

        return Vector3.negativeInfinity;
    }
}